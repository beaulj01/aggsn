import os,sys
import optparse
import logging

class RunnerConfig:
	def __init__( self ):
		self.__parseArgs( )

	def __parseArgs( self ):
		"""Handle command line argument parsing"""

		usage = """%prog [--help] [options] <inputs_file>

		This program will take a native aligned_reads.cmph5 and compare each molecule's
		kinetics profile with a matching WGA aligned_reads.cmp.h5. There are two protocols
		available for use within the pipeline, SMsn and SMp. The single argument for both 
		protocols is an inputs_file containing paths to the relevant files.

		The inputs_file has the following format:
		
		native_cmph5 : /path/to/native/aligned_reads.cmp.h5
		wga_cmph5    : /path/to/WGA/aligned_reads.cmp.h5     
		ref          : /path/to/sample/reference.fasta


		SMsn: Single-molecule, single nucleotide analysis
		Each motif site on each sequencing molecule is assessed for methylation
		status. This is designed for use with short (~250bp) sequencing library
		preps, where the long read lengths of SMRT reads enables multiple passes
		over each motif site. The reliability of the SMsn scores increases with
		more passes (i.e. higher single-molecule coverage).

		Example usage for SMsn analysis:
		smalr -i --SMsn --motif=GATC --mod_pos=1 --nat_lib=short --wga_lib=short --procs=4 -c 5 input.txt


		SMp: Single-molecule, motif-pooled analysis
		All motif sites on a sequencing molecule are pooled together and the
		molecule-wide methylation status for the given motif is assessed. This
		is designed for use with long (10Kb+) sequencing library preps, where each
		single long subread can span many distinct motif sites. The reliability of
		the SMp scores increases with increasing number of distinct motif sites
		contained in the subread.

		Example usage for SMp analysis:
		smalr -i --SMp --motif=GATC --mod_pos=1 --nat_lib=long --wga_lib=long --procs=4 -c 5 input.txt
		"""

		parser = optparse.OptionParser( usage=usage, description=__doc__ )

		parser.add_option( "-d", "--debug",           action="store_true", help="Increase verbosity of logging" )
		parser.add_option( "-i", "--info",            action="store_true", help="Add basic logging" )
		parser.add_option( "--logFile",               type="str", help="Write logging to file [log.out]" )
		parser.add_option( "--out",                   type="str", help="Filename to output SMsn/SMp results [<SMsn/SMp>.out]" )
		parser.add_option( "--capping",               action="store_true", help="Cap IPDs at the 95th percentile of global IPD distribution [False]" )
		parser.add_option( "--cap_percentile",        type="int", help="Percentile of global IPDs above which IPDs are capped [95]" )
		parser.add_option( "-n", "--nativeCovThresh", type="int", help="Aggregate native coverage threshold below which to skip analysis at that position [10]" )
		parser.add_option( "-w", "--wgaCovThresh",    type="int", help="Aggregate WGA coverage threshold below which to skip analysis at that position [10]" )
		parser.add_option( "-m", "--motif",           type="str", help="(Required) The sequence motif to be analyzed [None]" )
		parser.add_option( "-s", "--mod_pos",         type="int", help="(Required) The modified position (1-based) in the motif to be analyzed (e.g. for Gm6ATC, mod_pos=1) [None]" )
		parser.add_option( "--nat_lib",               type="str", help="String specifying the native sequencing library prep used. Either 'short' (for ~250bp) or 'long' (for 10Kb+). Used to manage memory consumption. [short]" )
		parser.add_option( "--wga_lib",               type="str", help="String specifying the WGA sequencing library prep used. Either 'short' (for ~250bp) or 'long' (for 10Kb+). Used to manage memory consumption. [short]" )
		parser.add_option( "--procs",                 type="int", help="Number of processors to use [4]" )
		parser.add_option( "--minSubreadLen",         type="int", help="Minimum length of a subread to analyze [100]" )
		parser.add_option( "--minAcc",                type="float", help="Minimum accuracy of a subread to analyze [0.8]" )
		parser.add_option( "--minMapQV",              type="int", help="Minimum mapQV of a subread to analyze [240]" )
		parser.add_option( "--leftAnchor",            type="int", help="Number of left bp to exclude around subread-level alignment errors [1]" )
		parser.add_option( "--rightAnchor",           type="int", help="Number of right bp to exclude around subread-level alignment errors [1]" )
		parser.add_option( "--N_reads",               type="int", help="Number of qualifying alignments to include in analysis [1000000000]" )
		parser.set_defaults( logFile="log.out",                \
							 debug=False,                      \
							 info=False,                       \
							 capping=False,                    \
							 cap_percentile=95,                \
							 out=".out",                       \
							 motif=None,                       \
							 mod_pos=None,                     \
							 nativeCovThresh=10,               \
							 wgaCovThresh=10,                  \
							 minSubreadLen=100,                \
							 minAcc=0.8,                       \
							 minMapQV=240,                     \
							 procs=4,                          \
							 wga_lib="short",                  \
							 nat_lib="short",                  \
							 leftAnchor=1,                     \
							 rightAnchor=1,                    \
							 N_reads=1000000000)

		self.opts, args = parser.parse_args( )
		if len(args) == 0:
			print usage
			sys.exit()

		# Shift the --mod_pos value into Pythonic 0-based indexing
		self.opts.mod_pos -= 1

		self.opts.out = "aggSN"+self.opts.out

		if self.opts.motif==None or self.opts.mod_pos==None:
			raise Exception("Please specify both the motif (--motif=<str>) and modified position in the motif (--mod_pos=<int>) to be analyzed!")

		if len(args) != 1:
			parser.error( "Expected 1 argument." )
		else:
			self.input_file = os.path.abspath(args[0])

		contig_dir = os.getcwd()
		orig_dir   = os.path.dirname(self.input_file)
		os.chdir(orig_dir)
		for line in open(self.input_file).xreadlines():
			if line.split(":")[0].strip()   == "ref":
				self.ref          = os.path.abspath(line.split(":")[1].strip())
			elif line.split(":")[0].strip() == "wga_cmph5":
				self.wga_cmph5    = os.path.abspath(line.split(":")[1].strip())
			elif line.split(":")[0].strip() == "native_cmph5":
				self.native_cmph5 = os.path.abspath(line.split(":")[1].strip())
			else:
				raise Exception("Unexpected field in the input file!\n%s" % line)
		os.chdir(contig_dir)
