import os,sys,shutil
import optparse
import logging
from pbcore.io.align.CmpH5IO import *
from itertools import groupby
from aggSN import AggSNRunner
import AggSNConfig
import operator

class aggSN_multicontig_runner:
	def __init__ ( self ):
		self.Config  = AggSNConfig.RunnerConfig( )
		print "Finished configuration..."

	def get_reference_contigs( self, cmph5 ):
		"""
		Pull out the list of contigs in the h5 file.
		"""
		reader  = CmpH5Reader(cmph5)
		contigs = []
		for ref in reader.referenceInfoTable:
			contigs.append( (ref[3], ref[2]) )
		return contigs

	def run( self ):
		print "Getting reference contigs..."
		nat_contigs  = self.get_reference_contigs(self.Config.native_cmph5)
		nat_contigs.sort(key=operator.itemgetter(1))

		print "Preparing to iterate over all contigs in %s" % self.Config.native_cmph5
		for nat_contig in nat_contigs:
			print "	- %s (%s)" % (nat_contig[1], nat_contig[0])

		abs_input_fn = os.path.abspath(self.Config.input_file)
		i            = 0
		for nat_contig in nat_contigs:
			dir_name = "%s_%s-%s_aggSN" % (nat_contig[1], self.Config.opts.motif, self.Config.opts.mod_pos)
			if os.path.exists(dir_name):
				shutil.rmtree(dir_name)
			os.mkdir(dir_name)
			os.chdir(dir_name)
			runner = AggSNRunner( i, nat_contig, abs_input_fn, self.Config )
			runner.run()
			os.chdir("../")
			i += 1

def main():
	app = aggSN_multicontig_runner()
	sys.exit( app.run() )

if __name__=="__main__":
	app = aggSN_multicontig_runner()
	sys.exit( app.run() )