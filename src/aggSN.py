import os,sys,time
import parse_mol_aligns
import subprocess
import logging
import numpy as np
from itertools import groupby
from collections import defaultdict, Counter
import math
from pbcore.io.align.CmpH5IO import *
import multiprocessing
import glob
import random
import copy
import operator
import AggSNConfig
import scipy.stats as stats

def run_command( CMD ):
	p         = subprocess.Popen(CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		for entry in stdOutErr:
			print entry
		raise Exception("Failed command: %s" % CMD)

def cat_list_of_files( in_fns, out_fn, header=None ):
	"""
	Given a list of filenames, cat them together into a single file. Then cleans up pre-catted
	single files.
	"""
	if len(in_fns)==0:
		return

	if header != None:
		f = open(out_fn,"w")
		f.write(header)
		f.close()
		cat_CMD  = "cat %s  >> %s" % (" ".join(in_fns), out_fn)
	else:
		cat_CMD  = "cat %s  > %s" % (" ".join(in_fns), out_fn)
	p         = subprocess.Popen(cat_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdOutErr = p.communicate()
	sts       = p.returncode
	if sts != 0:
		raise Exception("Failed cat command: %s" % cat_CMD)
	for fn in in_fns:
		try:
			os.remove(fn)
		except OSError:
			pass
	return out_fn

def fasta_iter(fasta_name):
	"""
	Given a fasta file, yield tuples of (header, sequence).
	"""
	fh = open(fasta_name)
	# ditch the boolean (x[0]) and just keep the header or sequence since
	# we know they alternate.
	faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
	for header in faiter:
		# drop the ">"
		header = header.next()[1:].strip()
		# join all sequence lines to one.
		seq = "".join(s.strip() for s in faiter.next())
		yield header, seq

class Consumer(multiprocessing.Process):
	def __init__(self, task_queue, result_queue, contig_id):
		multiprocessing.Process.__init__(self)
		self.task_queue   = task_queue
		self.result_queue = result_queue
		self.contig_id    = contig_id

	def run(self):
		proc_name = self.name
		while True:
			next_task = self.task_queue.get()
			if next_task is None:
				# Poison pill means shutdown
				logging.debug("%s - %s: Exiting" % (self.contig_id, proc_name))
				self.task_queue.task_done()
				break
			logging.debug("%s - %s: Starting" % (self.contig_id, proc_name))
			answer = next_task()
			self.task_queue.task_done()
			self.result_queue.put(answer)
		return

class AggSNRunner():
	def __init__ ( self, i, contig_info, abs_input_file, Config ):
		"""
		Parse the options and arguments, then instantiate the logger. 
		"""
		self.i                       = i
		self.Config                  = Config
		self.Config.opts.contig_name = contig_info[0]
		self.Config.opts.contig_id   = contig_info[1]
		self.__initLog( )
		
		logging.info("%s - contig_id:   %s" % (self.Config.opts.contig_id, self.Config.opts.contig_id))
		logging.info("%s - contig_name: %s" % (self.Config.opts.contig_id, self.Config.opts.contig_name))

		for i,entry in enumerate(fasta_iter(self.Config.ref)):
			self.ref_name = entry[0]
			if self.ref_name == self.Config.opts.contig_name:
				self.ref_size = len(entry[1])
			else:
				pass

	def __initLog( self ):
		"""Sets up logging based on command line arguments. Allows for three levels of logging:
		logging.error( ): always emitted
		logging.info( ) : emitted with --info or --debug
		logging.debug( ): only with --debug"""

		if os.path.exists(self.Config.opts.logFile):
			os.remove(self.Config.opts.logFile)

		logLevel = logging.DEBUG if self.Config.opts.debug \
					else logging.INFO if self.Config.opts.info \
					else logging.ERROR

		self.logger = logging.getLogger()
		self.logger.setLevel(logLevel)


		self.logger.handlers = []


		# create file handler which logs even debug messages
		fh = logging.FileHandler(self.Config.opts.logFile)
		fh.setLevel(logLevel)
		
		# create console handler with a higher log level
		ch = logging.StreamHandler()
		ch.setLevel(logLevel)
		
		# create formatter and add it to the handlers
		logFormat = "%(asctime)s [%(levelname)s] %(message)s"
		formatter = logging.Formatter(logFormat, datefmt='%H:%M:%S')
		ch.setFormatter(formatter)
		fh.setFormatter(formatter)
		
		# add the handlers to logger
		# if self.i == 0:
		self.logger.addHandler(ch)
		self.logger.addHandler(fh)
		logging.info("")
		logging.info("====================================")
		logging.info("Analyzing contig %s (%s)" % (self.Config.opts.contig_id, self.Config.opts.contig_name))
		logging.info("====================================")

	def find_motif_sites(self):
		"""
		This will write two files for a given motif, modification position in the motif, 
		and reference fasta. One file will have the motif positions on the forward strand
		and one on the negative strand of the reference.
		"""
		f_iter = fasta_iter(self.Config.ref)
		
		contig_fasta_fn = "%s.fasta" % self.Config.opts.contig_id
		f               = open(contig_fasta_fn, "w")
		for name,seq in f_iter:
			if name == self.Config.opts.contig_name:
				f.write(">%s\n" % name)
				f.write("%s" % seq)
		f.close()

		if not os.path.exists(contig_fasta_fn) or os.path.getsize(contig_fasta_fn)==0:
			raise Exception("Couldn't write the contig-specific fasta file!")

		caller_script    = os.path.join(os.path.dirname(__file__),'R/call_motifFinder.r')
		findMotif_script = os.path.join(os.path.dirname(__file__),'R/motifFinder.r')
		Rscript_CMD      = "Rscript %s %s %s %s %s" % (caller_script, findMotif_script, self.Config.opts.motif, self.Config.opts.mod_pos, contig_fasta_fn)
		p                = subprocess.Popen(Rscript_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdOutErr        = p.communicate()
		sts              = p.returncode
		if sts != 0:
			for entry in stdOutErr:
				print entry
			raise Exception("Failed command: %s" % Rscript_CMD)
		
		self.sites_pos = stdOutErr[0].split("\n")[0].split(" ")[1][1:-1]
		self.sites_neg = stdOutErr[0].split("\n")[1].split(" ")[1][1:-1]

	def launch_parallel_molecule_loading( self, cmph5_file ):
		"""
		"""
		logging.debug("Creating tasks...")
		tasks   = multiprocessing.JoinableQueue()
		results = multiprocessing.Queue()
		logging.debug("Done.")

		num_consumers = self.Config.opts.procs

		logging.debug("Starting consumers...")
		consumers     = [ Consumer(tasks, results, self.Config.opts.contig_id) for i in xrange(num_consumers) ]
		for w in consumers:
			w.start()
		logging.debug("Done.")

		def chunks( l, n ):
			"""
			Yield successive n-sized chunks from l.
			"""
			for i in xrange(0, len(l), n):
				yield l[i:i+n]

		# Enqueue jobs
		num_jobs       = self.Config.opts.procs
		N_target_reads = {}
		reads_left     = self.Config.opts.N_reads
		procs_left     = self.Config.opts.procs

		logging.info("Partitioning %s into %s chunks for analysis..." % (cmph5_file, num_jobs))
		reader         = CmpH5Reader(cmph5_file)
		alnIDs         = [r.AlnID for r in reader if r.referenceInfo[2]==self.Config.opts.contig_id]
		if len(alnIDs) <= num_jobs:
			num_jobs = 1
		reader.close()

		for job in range(num_jobs):
			N_target_reads[job] = int(math.ceil(float(reads_left)/procs_left))
			reads_left         -= N_target_reads[job]
			procs_left         -= 1

		import h5py
		h5py._errors.unsilence_errors() 

		
		chunksize    = int(math.ceil(float( len(alnIDs)/num_jobs )))
		align_chunks = list(chunks( alnIDs, chunksize ))
		for chunk_id in range(num_jobs):
			idx = align_chunks[chunk_id]
			n   = N_target_reads[chunk_id]
			tasks.put(parse_mol_aligns.agg_molecules_processor( cmph5_file,                   \
																chunk_id,                     \
																idx,                          \
																n,                            \
																self.ref_size,                \
																self.sites_pos,    \
																self.sites_neg,    \
																self.Config.opts))
		
		# Add a 'poison pill' for each consumer
		for i in xrange(num_consumers):
			tasks.put(None)
		
		# Wait for all of the tasks to finish
		tasks.join()
		
		# Start printing results
		parallel_results = []
		while num_jobs:
			result = results.get()
			parallel_results.append(result)
			num_jobs -= 1

		return parallel_results

	def run_ttest( self, a, b ):
		return stats.ttest_ind(a,b)

	def combine_chunk_ipdArrays( self, chunk_ipdArrays ):
		"""
		"""
		combined_ipdArray = {0:{}, 1:{}}
		for ipdArray in chunk_ipdArrays:
			for strand in ipdArray.keys():
				for pos in ipdArray[strand].keys():
					try:
						combined_ipdArray[strand][pos] = np.concatenate( (ipdArray[strand][pos], combined_ipdArray[strand][pos]) )
					except KeyError:
						combined_ipdArray[strand][pos] = ipdArray[strand][pos]
		return combined_ipdArray

	def run( self ):
		"""
		Execute the pipeline.
		"""
		self.find_motif_sites()

		logging.info("Analyzing WGA alignments in %s..." % self.Config.wga_cmph5)
		chunk_ipdArrays = self.launch_parallel_molecule_loading( self.Config.wga_cmph5 )
		logging.info("Done.")

		logging.info("%s - Combining the %s separate ipdArray dictionaries..." % (self.Config.opts.contig_id, len(chunk_ipdArrays)))
		control_ipds    = self.combine_chunk_ipdArrays( chunk_ipdArrays )
		logging.debug("%s - Done." % self.Config.opts.contig_id)

		logging.info("Analyzing native alignments in %s..." % self.Config.native_cmph5)
		chunk_ipdArrays = self.launch_parallel_molecule_loading( self.Config.native_cmph5 )
		logging.info("Done.")

		logging.info("%s - Combining the %s separate ipdArray dictionaries..." % (self.Config.opts.contig_id, len(chunk_ipdArrays)))
		native_ipds    = self.combine_chunk_ipdArrays( chunk_ipdArrays )
		logging.debug("%s - Done." % self.Config.opts.contig_id)

		logging.info("Comparing native and control IPDs at each %s-%s position..." % (self.Config.opts.motif, self.Config.opts.mod_pos))
		f = open(self.Config.opts.out, "w")
		for strand,pos_dict in native_ipds.iteritems():
			for pos,nat_ipds in native_ipds[strand].iteritems():
				if control_ipds[strand].get(pos, "nodata")=="nodata":
					continue
				else:
					nat_cov = len(nat_ipds)
					wga_cov = len(control_ipds[strand][pos])
					
					if nat_cov < self.Config.opts.nativeCovThresh or wga_cov < self.Config.opts.wgaCovThresh:
						continue
					
					wga_ipds = np.array(control_ipds[strand][pos])
					nat_ipds = np.array(nat_ipds)
					
					if self.Config.opts.capping:
						wga_cap  = np.percentile(wga_ipds, self.Config.opts.cap_percentile)
						nat_cap  = np.percentile(nat_ipds, self.Config.opts.cap_percentile)
						wga_ipds = np.clip(wga_ipds, -1000, wga_cap)
						nat_ipds = np.clip(nat_ipds, -1000, nat_cap)

					t,p      = self.run_ttest(nat_ipds, wga_ipds)

					line = "%s\t%s\t%.3f\t%s\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%s\t%s" % (strand,                            \
															     		   			   pos,                               \
															     		   			   t,                                 \
															     		   			   p,                                 \
															     		   			   nat_ipds.mean() - wga_ipds.mean(), \
															     		   			   nat_ipds.mean(),                   \
															     		   			   wga_ipds.mean(),                   \
																		   			   nat_ipds.std(),                    \
																		   			   wga_ipds.std(),                    \
															     		   			   len(nat_ipds),                     \
															     		   			   len(wga_ipds))
					f.write(line+"\n")
		f.close()
		logging.info("Done.")

		logging.info("Sorting output by strand/position...")
		sort_CMD = "sort -t$'\t' -nk2 %s > sorting.tmp" % self.Config.opts.out
		p         = subprocess.Popen(sort_CMD, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		stdOutErr = p.communicate()
		sts       = p.returncode
		if sts != 0:
			raise Exception("Failed sort command: %s" % sort_CMD)
		os.rename("sorting.tmp", self.Config.opts.out)
		logging.info("Done.")

def main():
	app = AggSNRunner()
	sys.exit( app.run() )
