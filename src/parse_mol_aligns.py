import sys,os
import shutil
import glob
import numpy as np
import math
from collections import defaultdict,Counter
from pbcore.io.align.CmpH5IO import *
import logging
import subprocess
import random
import copy
from operator import itemgetter

class ipd_entry:
	def __init__( self, tup ):
		"""
		"""
		self.base    = tup[0]
		self.pos     = tup[1]
		self.call    = tup[2]
		self.ipd     = tup[3]
		self.strand  = np.abs(tup[4]-1)
		# self.subread = tup[5]

class subread:
	def __init__( self, cmph5, alignment, leftAnchor, rightAnchor ):
		self.entries = {}

		self.subname    = alignment.readName
		movieID         = alignment.movieInfo[0]
		alignedLength   = alignment.referenceSpan
		fps             = alignment.movieInfo[2]
		self.refName    = alignment.referenceInfo[3]
		zmw             = alignment.HoleNumber
		self.mol        = alignment.MoleculeID
		if alignment.isForwardStrand:
			self.strand = 0
		else:
			self.strand = 1
		ref_bases  = alignment.reference()
		read_bases = alignment.read()
		read_calls      = alignment.transcript()
		ref_pos         = list(alignment.referencePositions())
		IPD             = list(alignment.IPD())
		# label           = self.opts.cmph5_labels[cmph5]

		error_mk = []
		for read_call in read_calls:
			# Go through all entries and flag which positions are MM/indels
			if read_call != "M":
				# Mismatch or indel at this position!
				error_mk.append(1)
			else:
				error_mk.append(0)
		
		# Get the indices of all the non-matches
		error_idx = [i for (i,val) in enumerate(error_mk) if val == 1]
		for error_id in error_idx:
			try:
				for j in range(leftAnchor):
					error_mk[error_id - (j+1)] = 1
				for j in range(rightAnchor):
					error_mk[error_id + (j+1)] = 1
			except IndexError:
				pass
		error_mk = np.array(error_mk)

		ipds     = np.array(IPD) / fps
		
		strands  = np.array([self.strand] * len(read_calls))
		# subread  = np.array([subread_id]  * len(read_calls))

		ref_bases  = np.array(list(ref_bases))
		read_bases = np.array(list(read_bases))
		ref_pos    = np.array(ref_pos)
		read_calls = np.array(list(read_calls))

		# Mark the error positions, but leave them in the sequence so
		# we can pull out intact motifs from contiguous correct bases
		ref_bases[error_mk==1]  = "*"
		read_bases[error_mk==1] = "*"
		read_calls[error_mk==1] = "*"
		ipds[error_mk==1]       = -9
		# strands[error_mk==1]    = -9
		# subread[error_mk==1]    = -9

		# Attach these IPD entries to the subread object
		for tup in zip(ref_bases, ref_pos, read_calls, ipds, strands):
			entry                                     = ipd_entry(tup)
			self.entries[ (entry.pos, entry.strand) ] = entry

		self.strand = self.entries.values()[0].strand

		# self.cap_outliers()
		self.subread_normalize()

	def cap_ipd_vals( self, cap_value ):
		for key,entry in self.entries.iteritems():
			entry.ipd = min(entry.ipd, cap_value)

	def subread_normalize( self ):
		"""
		Every IPD entry needs to be normalized by the mean IPD of its subread.
		"""
		if len(self.entries) == 0:
			# Nothing to do here.
			return self.entries

		# First populate list of all IPDs per subread. Will use to get normalization factor.
		subread_vals = []
		for entry in self.entries.values():
			# Only do if this IPD is NOT from an error position
			if entry.ipd != -9:
				subread_vals.append(entry.ipd)

		rawIPDs = np.array(map(lambda x: math.log(x + 0.001), subread_vals))
		nfs     = rawIPDs.mean()

		for pos, entry in self.entries.iteritems():
			if entry.ipd == -9:
				newIPD = -9
			else:
				newIPD = math.log(entry.ipd + 0.001) - nfs
			
			entry.ipd = newIPD

def read_in_motif_sites( motifSites_fn ):
	"""
	"""
	sites = []
	for line in open(motifSites_fn).xreadlines():
		sites.append(int(float(line.strip())))
	return sites

def generate_molecule_ZMW_map( mols, chunk_id ):
	"""
	Map zmw_id <--> mol_id. This will also remove any double-loaded molecules.
	"""
	zmw_mol_map    = {}
	zmw_mol_counts = Counter()
	for mol in mols.values():
		zmw_mol_map[mol.mol_id] = mol.zmw_id
		zmw_mol_counts[mol.zmw_id] += 1

	mol_ids              = zmw_mol_map.keys()
	multi_loaded_mol_ids = [mol_id for mol_id in mol_ids if zmw_mol_counts[zmw_mol_map[mol_id]] > 1]
	mols            = dict([ (mol.mol_id, mol) for mol in mols.values() if mol.mol_id not in multi_loaded_mol_ids])
	try:
		logging.debug("Process %s: removed %s double-loaded molecules (%.2f%%)." % (chunk_id,                  \
																			  		len(multi_loaded_mol_ids), \
																			  		100*len(multi_loaded_mol_ids)/float(len(mols.values()))))
	except ZeroDivisionError:
		logging.debug("Process %s: no molecules available for ZMW/moleculeID mapping!" % chunk_id)
	return mols, zmw_mol_map

def remove_split_up_molecules( mols, split_mols ):
	"""
	A few molecules have their alignments split between processes, so split_mols
	keeps track of these and now we'll exclude them from further analysis. This
	is a stop-gap measure until we can figure out a quick way of splitting the 
	original alignments file more cleverly.
	"""
	all_mols = set([mol.mol_id for mol in mols.values()])
	to_del   = list(set.intersection(split_mols, all_mols))
	for mol in to_del:
		del mols[mol]
	return mols

def remove_nonmotif_entries( ipdArrays, sites_pos, sites_neg, left=10, right=10 ):
	"""
	Go through the ipdArrays and only keep those that correspond to a motif site.
	"""
	ipdArrays_to_keep  = {0:{}, 1:{}}
	motif_pos_set_plus = set(sites_pos)
	ipds_pos_set_plus  = set(ipdArrays[0].keys())
	overlap_plus       = list(motif_pos_set_plus & ipds_pos_set_plus)
	if len(overlap_plus)>0:
		for p in overlap_plus:		
			ipdArrays_to_keep[0][p] = ipdArrays[0][p]

	motif_pos_set_minus = set(sites_neg)
	ipds_pos_set_minus  = set(ipdArrays[1].keys())
	overlap_minus       = list(motif_pos_set_minus & ipds_pos_set_minus)
	if len(overlap_minus)>0:
		for p in overlap_minus:		
			ipdArrays_to_keep[1][p] = ipdArrays[1][p]

	return ipdArrays_to_keep

def alignment_file_len( alignments_flat_fn ):
	with open(alignments_flat_fn) as f:
		for i, l in enumerate(f):
			pass
	return i + 1



class agg_molecules_processor:
	def __init__( self,               \
				  cmph5,              \
				  chunk_id,           \
				  idx,                \
				  n,                  \
				  ref_size,           \
				  sites_pos,          \
				  sites_neg,          \
				  opts ):
			
		self.cmph5              = cmph5
		self.chunk_id           = chunk_id
		self.idx                = idx
		self.N_target_reads     = n
		self.ref_size           = ref_size
		self.sites_pos_fn       = sites_pos
		self.sites_neg_fn       = sites_neg
		self.opts               = opts
		self.leftAnchor         = opts.leftAnchor
		self.rightAnchor        = opts.rightAnchor
		self.nat_lib            = opts.nat_lib
		self.wga_lib            = opts.wga_lib

		if self.sites_pos_fn != None and self.sites_neg_fn != None:
			logging.debug("Process %s: reading motif sites..." % self.chunk_id)
			self.sites_pos = read_in_motif_sites( self.sites_pos_fn  )
			self.sites_neg = read_in_motif_sites( self.sites_neg_fn  )

	def __call__( self ):
		reader = CmpH5Reader(self.cmph5)
		self.ipdArrays = {0:defaultdict(list), 1:defaultdict(list)}
		try:
			for i,alignment in enumerate(reader[self.idx]):
				to_get = min(self.N_target_reads, len(self.idx))
				incr   = max(4,to_get/4)
				if i%incr==0:
					logging.info("...chunk %s - alignment %s/%s (%.1f%%)" % (self.chunk_id, i, to_get, 100*i/to_get))
				
				if alignment.MapQV >= self.opts.minMapQV and alignment.accuracy >= self.opts.minAcc and alignment.referenceSpan >= self.opts.minSubreadLen:
					sub     = subread( self.cmph5, alignment, self.leftAnchor, self.rightAnchor )
					
					sub_pos = [entry.pos for entry in sub.entries.values()]
					if sub.strand==0:
						motif_positions = set(self.sites_pos) & set(sub_pos)
					else:
						motif_positions = set(self.sites_neg) & set(sub_pos)

					for position in motif_positions:
						entry = sub.entries[position, sub.strand]
						if entry.ipd!=-9.0:
							self.ipdArrays[entry.strand][position].append(entry.ipd)
				if i==self.N_target_reads:
					break
		except IndexError:
			logging.warning("%s IndexError:Is this a bug in the cmph5 API?" % i)
		reader.close()
		return self.ipdArrays

	def create_agg_IPD_arrays( self ):
		"""
		Given a set of molecule object, each with a set of molecule identifiers
		and entries (containing IPD, base, pos, strand, call, subread), construct
		a dictionary of arrays that aggregate all the molecules together.
		"""
		tmp_ipdArrays  = {0:defaultdict(list), 1:defaultdict(list)}
		self.ipdArrays = {0:{}, 1:{}}
		for mol in self.mols.values():
			for entry in mol.entries.values():
				tmp_ipdArrays[entry.strand][entry.pos].append(entry.ipd)

		for strand in tmp_ipdArrays.keys():
			for pos in tmp_ipdArrays[strand].keys():
				if len(tmp_ipdArrays[strand][pos]) > 0:
					self.ipdArrays[strand][pos] = np.array(tmp_ipdArrays[strand][pos])