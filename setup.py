from setuptools import setup, Extension, find_packages
import sys

if ("install" in sys.argv) and sys.version_info < (2, 7, 0):
		print "AggSN requires Python 2.7"
		sys.exit(-1)

globals = {}
execfile("src/__init__.py", globals)
__VERSION__ = globals["__VERSION__"]

setup(
		name = 'AggSN',
		version=__VERSION__,
		author='John Beaulaurier',
		author_email='john.beaulaurier@mssm.edu',
		description='',
		url='https://bitbucket.org/beaulj01/aggsn/',
		
		packages= ['aggSN'],
		package_dir= {'aggSN' : 'src'},
		package_data= {'aggSN': ['R/*.r']},
		zip_safe= False,
		scripts= ['bin/aggSN',
			  'bin/call_aggSN.py'],
		install_requires=['pbcore >= 0.9.4',
				  'numpy >= 1.6.0',
				  'cython >= 0.17',
				  'scipy >= 0.13'])
