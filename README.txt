############
Installation:
############

(if running on Minerva, first execute 'module load python R gcc')

1. Create and activate a python virtual environment with virtualenv
   - See http://docs.python-guide.org/en/latest/dev/virtualenvs/
2. Install package dependencies inside your virtual environment
   - numpy >= 1.9.1 ('pip install numpy')
   - cython >= 0.17 ('pip install cython')
   - h5py >= 2.4.0 ('pip install h5py')
   - pbcore >= 0.9.4 (https://github.com/PacificBiosciences/pbcore)
     * Requires Python >= 2.7
3. Clone aggSN source code from Bitbucket to a local repository
   - https://bitbucket.org/beaulj01/aggsn
4. Install aggSN inside your virtual environment
   - cd  <aggSNR repository>
   - python setup.py install
5. Confirm installation by testing both SMsn and SMp protocols 
   - cd test
   - sh run.sh
     * Should generate the folder ref000001_GATC-1_aggSN, containing pipeline output

############
Pipeline input
############
AggSN requires an input_files.txt argument:
   - Specifies relevant file paths in the following format:
      native_cmph5: <path to native cmp.h5>
      wga_cmph5:    <path to WGA cmp.h5> (optional, can specify native cmp.h5 if not 
                    available)
      ref:          <path to reference that matches that used in the cmp.h5 files>

############
Pipeline output
############
One output directory will be created for each contig in the reference. If there is
only one contig, the results will be placed in ref000001. These results include a 
log detailing the analysis of that contig, the motif positions in that contig (forward 
and reverse strand), a fasta file of that contig, and a results file (aggSN.out). 
This results file contains the following informtation:

Column  Meaning
1 	strand
2 	position
3 	t-statistic
4 	t-test p-value
5 	IPD ratio ( mean(ln[native]) - mean(ln[WGA]) )
6 	mean(ln[native])
7 	mean(ln[WGA])
8 	native coverage
9 	WGA coverage
